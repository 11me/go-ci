FROM alpine:latest

RUN apk update && apk add openssh

CMD ["/bin/sh", "-c", "echo Hello"]
