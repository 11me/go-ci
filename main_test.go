package main

import "testing"

func TestSayHi(t *testing.T) {
	got := SayHi("John")
	want := "Hello, John!"

	if got != want {
		t.Errorf("want %s, got %s", want, got)
	}
}
