package main

import "fmt"

func SayHi(name string) string {
	return fmt.Sprintf("Hello, %s!", name)
}

func main() {
	SayHi("John")
}
